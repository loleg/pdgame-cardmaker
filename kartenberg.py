#!/usr/bin/env python
"""

Generate a playing deck of cards for the Public Domain Game
http://make.opendata.ch/wiki/project:publicdomaingame

Requires Inkscape, pdfjam and several Python modules
See README.md for details

"""
SVG_TEMPLATES = {
    'aktion': "templates/card-aktion.svg",
    'remix':  "templates/card-remix.svg",
    'qr':     "templates/card-qr.svg",
}
INKSCAPE_PATH = "/usr/bin/inkscape"
INKSCAPE_L1 = 722.36218
MAX_ROWS_LOAD = 1000

REQUIRED_FIELDS = ['Name', 'Kategorie Werk']
BLANK_FIELDS = ['Werkbeschreibung', 'Link']

SOURCE_FILE_XLS = 'NEU Werke von Autoren gestorben 1940-45.xlsx'
ACTION_FILE_XLS = 'Spiel und weitere Karten.xlsx'

TARGET_BASE_URL = 'http://go.soda.camp/pdg/'

DEBUG_SKIP_ACTION = False
DEBUG_SKIP_WERKE = True 

from identifier import (getIdentifier, getShortText)

import string
import subprocess
import pandas as pd
from os import mkdir, path
from tempfile import mkdtemp
from lxml import etree
from six import BytesIO
import qrcode
import qrcode.image.svg

qr_factory = qrcode.image.svg.SvgPathImage

def main():
    # Load the base template
    svg_template = {}
    for t in SVG_TEMPLATES:
        with open(SVG_TEMPLATES[t], 'r') as svgfile:
            svg_template[t] = etree.parse(svgfile)
    print "Template loaded."

    # Load from Excel exports
    if DEBUG_SKIP_ACTION:
        action_obj = []
    else:
        action_obj = getActionCardsFromExcel(path.join('data', ACTION_FILE_XLS))
    print "%d actions loaded." % len(action_obj)

    if DEBUG_SKIP_WERKE:
        art_obj = []
    else:
        art_obj = getArtCardsFromExcel(path.join('data', SOURCE_FILE_XLS))
    print "%d objects loaded." % len(art_obj)

    data_obj = action_obj + art_obj
    print "%d total data rows loaded." % len(data_obj)

    temp_output = mkdtemp(prefix='kartenberg-')
    print "Writing to %s" % temp_output

    i = 0
    page = 1
    filelist = []
    for w in data_obj:
        # print w # Card details in log
        i = i + 1
        typ = w['_type']
        cat = getShortText(w['category'])
        # Convert identifier to (QR code) URL
        if w['_id'] is not None:
            linkurl = TARGET_BASE_URL + "/".join(w['_id'])
        else:
            linkurl = None
        # Advance new page after a certain amount
        # if i % 10 == 0: 
        #     page = page + 1
        # We are not using pages, but categories now
        target_folder = path.join(temp_output, "pdg-%s-%s" % (typ, cat))
        if not path.exists(target_folder):
            mkdir(target_folder)
        filename = path.join(temp_output, target_folder, "card-%d" % i)
        filelist.append(filename + '.pdf')
        print "Writing %s -> %s" % (filename, linkurl)
        metacard = {
            "title":    w['title'],
            "author":   w['author'],
            "text":     w['text'],
            "link":     linkurl,
            "output":   filename
        }
        if linkurl is None:
            makeCard(svg_template[cat], metacard)
        else:
            makeCard(svg_template['qr'], metacard)

    combinePDFtoPage(filelist, temp_output)

def getArtCardsFromExcel(filename):
    timeperiods = pd.ExcelFile(filename)
    werke = []
    data_werke = []
    for timeperiod_name in timeperiods.sheet_names:
        timeperiod = timeperiods.parse(timeperiod_name)
        # Filter out the "Unnamed" column
        timeperiod = timeperiod[list(filter(lambda x: 'Unnamed' not in x, timeperiod.columns))]
        valid_entries = filter(lambda x: not (all(pd.isnull(x))), timeperiod.get_values())

        for entry in map(lambda artifact: dict(zip(timeperiod.columns, artifact.tolist())), valid_entries):
            # Set nulls to None and filter required fields
            skip_entry = False
            for fld in REQUIRED_FIELDS:
                if not fld in entry:
                    skip_entry = True
            for prop in entry:
                if pd.isnull(entry[prop]):
                    entry[prop] = ""
                    if prop in REQUIRED_FIELDS:
                        skip_entry = True
            # Check for unique record
            identifier = getIdentifier(entry['Name'], entry['Werk'])
            if not identifier in werke and not skip_entry:
                for f in BLANK_FIELDS:
                    if not f in entry:
                        entry[f] = ""
                werke.append(identifier)
                # Convert diverging column names
                place_of_death = getFieldIn(entry, ['Gestorben in', 'Todesort'], " in %s")
                if place_of_death is None: continue
                # Add to collection
                data_werke.append({
                    'title': entry['Werk'],
                    'category': entry['Kategorie Werk'],
                    'author': u"%s \u2020 %s%s" % (
                        unicode(entry['Name']),
                        unicode(entry['Todesdatum']),
                        unicode(place_of_death)
                    ),
                    'text': u" ".join([
                        unicode(entry['Werkbeschreibung']),
                        unicode(entry['Archiv']),
                        unicode(entry['Link']),
                    ]).replace("  ", " ").strip(),
                    '_entry': entry,
                    '_id': identifier,
                    '_type': 'werke'
                })
                if len(data_werke) == MAX_ROWS_LOAD:
                    return data_werke
    return data_werke

def getActionCardsFromExcel(filename):
    spreadsheet = pd.ExcelFile(filename)
    uniques = []
    data_items = []
    for worksheet_name in spreadsheet.sheet_names:
        if not 'Remixes' in worksheet_name and not 'Aktionen' in worksheet_name: continue
        worksheet = spreadsheet.parse(worksheet_name)
        valid_entries = filter(lambda x: not (all(pd.isnull(x))), worksheet.get_values())

        for entry in map(lambda artifact: dict(zip(worksheet.columns, artifact.tolist())), valid_entries):
            # Set nulls to None and filter required fields
            for prop in entry:
                if pd.isnull(entry[prop]):
                    entry[prop] = ""
            if 'Remixes' in worksheet_name:
                if not 'Titel' in entry or entry['Titel'] is None: continue
                # Check for unique record
                identifier = unicode(entry['Titel'])
                if not 'remix-' + identifier in uniques:
                    uniques.append('remix-' + identifier)
                    # Add to collection
                    data_items.append({
                        'title': identifier,
                        'author': unicode(entry['Name']),
                        'text': u" ".join([
                            "[%s]" % unicode(entry['Genre']),
                            unicode(entry['Tool / Remix']),
                            unicode(entry['Web']),
                        ]).replace("  ", " ").replace("[] ", "").strip(),
                        '_entry': entry,
                        '_id': None, # no link
                        '_type': 'tool',
                        'category': 'remix',
                    })
            else:
                if not 'Beschreibung' in entry or entry['Beschreibung'] is None: continue
                # Check for unique record
                identifier = unicode(entry['Beschreibung'])
                if not 'aktion-' + identifier in uniques:
                    uniques.append('aktion-' + identifier)
                    # Add to collection
                    data_items.append({
                        'title':    identifier,
                        'author':   unicode(entry['Aktion']),
                        'text':     unicode(entry['Link']),
                        '_entry': entry,
                        '_id': None, # no link
                        '_type': 'tool',
                        'category': 'aktion',
                    })
            if len(data_items) == MAX_ROWS_LOAD:
                return data_items
    return data_items

def getFieldIn(entry, listFields, stringForm="%s"):
    pod = 0
    for f in listFields:
        if f in entry:
            pod = entry[f]
    if pod == 0: return None
    if pod is None or pod == "":
        return ""
    else:
        return stringForm % pod

def makeQRCode(text):
    """ Get an SVG tree from a QR code """
    qr = qrcode.QRCode(
        version=3,
        error_correction=qrcode.constants.ERROR_CORRECT_M,
        box_size=20,
        border=4,
    )
    qr.add_data(text)
    qr.make(fit=False)
    qr_code_img = qr.make_image(image_factory=qr_factory)
    svg_buffer = BytesIO()
    qr_code_img.save(svg_buffer)
    svg = etree.XML(svg_buffer.getvalue())
    return svg.getchildren()[0]

def makeCard(svg_tree, card):
    # Modify fields
    getParaById(svg_tree, 'cardtitle')[0].text  = card['title']
    getParaById(svg_tree, 'cardauthor')[0].text = card['author']
    getParaById(svg_tree, 'cardtext')[0].text   = card['text']
    if card['link'] is not None:
        # Clear previous QR codes
        cardrect = svg_tree.xpath("//n:rect[@id='cardcode']",
            namespaces={'n': "http://www.w3.org/2000/svg"})[0]
        qrParent = cardrect.getparent().getparent()
        for prevpath in qrParent.xpath("svg:path",
            namespaces={'svg': "http://www.w3.org/2000/svg"}):
            prevpath.getparent().remove(prevpath)
        # Apply QR code to the rectangle in the template
        qrcode = makeQRCode(card['link'])
        x = float(cardrect.get('x')) - 7
        y = float(cardrect.get('y')) - INKSCAPE_L1 - 7
        s = 1
        qrcode.set('transform', "translate(%d, %d) scale(%d)" % (x, y, s))
        qrParent.append(qrcode)
    # Generate outputs
    saveToSVG(svg_tree, card['output'])
    convertSVGtoPDF(card['output'])

def saveToSVG(svg_tree, filename):
    """ Writes tree to file """
    with open(filename + ".svg", 'w') as f:
        f.write(etree.tostring(svg_tree))

def convertSVGtoPDF(filename):
    """ Executes Inkscape to create a PDF """
    p = subprocess.call([
        INKSCAPE_PATH,  filename + ".svg",
        '--export-pdf', filename + ".pdf"
    ])
    if p != 0:
        print "Error running Inkscape!"

def combinePDFtoPage(sourcefiles, outputpath):
    """ Uses pdfjam to create a montage """
    p = subprocess.call(['sh', 'montage.sh', outputpath])
    if False:
        p = subprocess.call([
            'pdfjam', 
            '--quiet',
            '--nup 3x3', # tile layout
            "--outfile %s.pdf" % outputpath
        ] + sourcefiles # source files or folder
        )
    if p != 0:
        print "Error running pdfjam!"

def getParaById(svg_tree, id):
    return svg_tree.xpath("//n:flowRoot[@id='%s']/n:flowPara" % id,
        namespaces={'n': "http://www.w3.org/2000/svg"})

main()
