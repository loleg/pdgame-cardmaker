cd $1
for f in * ; do
	if [ -d $f ]; then
		echo "Processing page $f"
		cd $f
		pdfjam --nup 3x3 --outfile montage.pdf -- *.pdf
		cd ..
		mv $f/montage.pdf $f-montage.pdf
		rm -rf $f
	fi
done
echo "Montage complete in $1"